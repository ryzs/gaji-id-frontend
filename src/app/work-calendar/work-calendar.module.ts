import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule, MatExpansionModule, MatSelectModule, MatFormFieldModule, MatNativeDateModule, MatAutocompleteModule, MatButtonModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { WorkCalendarRoutingModule } from './work-calendar-routing.module';

import { WorkCalendarComponent } from './work-calendar/work-calendar.component';
import { WorkCalendarListComponent } from './work-calendar-list/work-calendar-list.component';
import { WorkCalendarDetailComponent } from './work-calendar-detail/work-calendar-detail.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [WorkCalendarComponent, WorkCalendarListComponent, WorkCalendarDetailComponent],
  imports: [
    CommonModule,
    WorkCalendarRoutingModule,
    MatInputModule,
    MatExpansionModule,
    MatSelectModule,
    MatFormFieldModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule
  ]
})
export class WorkCalendarModule { }
