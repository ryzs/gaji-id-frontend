import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorkCalendarComponent } from './work-calendar/work-calendar.component';
import { WorkCalendarListComponent } from './work-calendar-list/work-calendar-list.component';
import { WorkCalendarDetailComponent } from './work-calendar-detail/work-calendar-detail.component';

const routes: Routes = [
    {
        path: 'work-calendar',
        component: WorkCalendarComponent,
        children: [
            {
                path: '',
                component: WorkCalendarListComponent
            },
            {
                path: 'detail',
                component: WorkCalendarDetailComponent
            }
        ]
    }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkCalendarRoutingModule { }
