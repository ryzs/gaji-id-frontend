import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-work-calendar-list',
  templateUrl: './work-calendar-list.component.html',
  styleUrls: ['./work-calendar-list.component.css']
})
export class WorkCalendarListComponent implements OnInit {

  constructor(private formBuilder: FormBuilder) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  filterForm: FormGroup;
  submitted: boolean = false;
  displayedColumns: string[] = ['employeeId', 'employeeName', 'jlmHadir', 'jmlCuti', 'jmlLembur'];
  absenceList = new MatTableDataSource<any>([
    { employeeId: 1, employeeName: 'John Hydrogen', jlmHadir: 1, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 2, employeeName: 'John Helium', jlmHadir: 4, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 3, employeeName: 'John Lithium', jlmHadir: 6, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 4, employeeName: 'John Beryllium', jlmHadir: 9, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 5, employeeName: 'John Boron', jlmHadir: 10, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 6, employeeName: 'John Carbon', jlmHadir: 12, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 7, employeeName: 'John Nitrogen', jlmHadir: 14, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 8, employeeName: 'John Oxygen', jlmHadir: 15, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 9, employeeName: 'John Fluorine', jlmHadir: 18, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 10, employeeName: 'John Neon', jlmHadir: 20, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 11, employeeName: 'John Sodium', jlmHadir: 22, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 12, employeeName: 'John Magnesium', jlmHadir: 24, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 13, employeeName: 'John Aluminum', jlmHadir: 26, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 14, employeeName: 'John Silicon', jlmHadir: 28, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 15, employeeName: 'John Phosphorus', jlmHadir: 30, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 16, employeeName: 'John Sulfur', jlmHadir: 32, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 17, employeeName: 'John Chlorine', jlmHadir: 35, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 18, employeeName: 'John Argon', jlmHadir: 39, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 19, employeeName: 'John Potassium', jlmHadir: 39, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
    { employeeId: 20, employeeName: 'John Calcium', jlmHadir: 40, jmlCuti: Math.random()*100, jmlLembur: Math.random()*100 },
  ]);

  employees: any[] = [
    {
      id: 1,
      nik: 2312123,
      employeeName: 'Budi'
    },
    {
      id: 2,
      nik: 2312123,
      employeeName: 'Firman'
    },
    {
      id: 3,
      nik: 2312123,
      employeeName: 'Angga'
    }
  ];
  workplaces = [];
  filteredOptions: Observable<string[]>;

  ngOnInit() {
    this.filterForm = this.formBuilder.group({
      workplaceId: [],
      dateFrom: [],
      dateTo: [],
      employeeId: []
    });

    this.filteredOptions = this.filterForm.controls.employeeId.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.employees.filter(option => option.employeeName.toLowerCase().includes(filterValue));
  }



  onSearch() {
    this.submitted = true;
    console.log(this.filterForm.value)
  }

}
